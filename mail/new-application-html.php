<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/login']);
?>
<div class="password-reset">
    <p>Hello Admin!,</p>

    <p>There is a new application submitted to you.</p>

    <p> Click here <?= Html::a(Html::encode($resetLink), $resetLink) ?> to login and check it out</p>
    <p>
        Kind regards,<br>
        Seeta System
    </p>
</div>
