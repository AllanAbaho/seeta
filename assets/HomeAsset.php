<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class HomeAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "css/assets/bootstrap.min.css",
        "css/assets/font-awesome.min.css",    
        "css/assets/flaticon.css",
        "css/assets/magnific-popup.css",    
        "css/assets/owl.carousel.css",
        "css/assets/owl.theme.css",     
        "css/assets/animate.css", 
        "css/assets/slick.css",  
        "css/assets/preloader.css",    
        "css/assets/revolution/layers.css",
        "css/assets/revolution/navigation.css",
        "css/assets/revolution/settings.css",    
        "css/assets/meanmenu.css",
        "css/style.css",    
        "css/responsive.css",
        "css/demo.css",    
        'css/site.css',
        'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css',
    ];
    public $js = [
        "js/jquery-3.2.1.min.js",
        "js/popper.min.js",
        "js/bootstrap.min.js",
        "js/assets/revolution/jquery.themepunch.revolution.min.js",
        "js/assets/revolution/jquery.themepunch.tools.min.js", 
        "js/jquery.magnific-popup.min.js",     
        "js/owl.carousel.min.js",   
        "js/slick.min.js",   
        "js/jquery.meanmenu.min.js",  
        "js/waypoints.min.js",
        "js/jquery.counterup.min.js",    
        "js/wow.min.js", 
        "js/assets/revolution/extensions/revolution.extension.actions.min.js",
        "js/assets/revolution/extensions/revolution.extension.carousel.min.js",
        "js/assets/revolution/extensions/revolution.extension.kenburn.min.js",
        "js/assets/revolution/extensions/revolution.extension.layeranimation.min.js",
        "js/assets/revolution/extensions/revolution.extension.migration.min.js",
        "js/assets/revolution/extensions/revolution.extension.navigation.min.js",
        "js/assets/revolution/extensions/revolution.extension.parallax.min.js",
        "js/assets/revolution/extensions/revolution.extension.slideanims.min.js",
        "js/assets/revolution/extensions/revolution.extension.video.min.js",
        "js/assets/revolution/revolution.js",
        "js/custom.js", 
        "js/demo.js",    
        'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );
}
