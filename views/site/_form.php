<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Intakes;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Intakes */
/* @var $form yii\widgets\ActiveForm */
$intakes = ArrayHelper::map(Intakes::find()->all(), 'id', 'name');
?>

<div class="intakes-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'intake_id')->dropDownList($intakes) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <label class="control-label" for="applications-gender">Date Of Birth</label>
            <div class="start_date input-group mb-4">
                <?php if($model->id): ?>   
                    <input class="form-control start_date" type="text" placeholder="Date Of Birth" name="Applications[dob]" id="startdate_datepicker" value="<?=$model->dob; ?>">
                <?php else: ?>
                    <input class="form-control start_date" type="text" placeholder="Date Of Birth" name="Applications[dob]" id="startdate_datepicker">
                <?php endif; ?>
                <div class="input-group-append">
                <span class="fa fa-calendar input-group-text start_date_calendar" aria-hidden="true "></span>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'gender')->dropDownList(['Male' => 'Male','Female' => 'Female']) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'religion')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'nationality')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'home_address')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'guardian_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'guardian_occupation')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'guardian_contact')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'status')->hiddenInput(['value' => 'Review'])->label(false) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    $("#startdate_datepicker").datepicker({
        format: 'yyyy-mm-dd',
    });
</script>
