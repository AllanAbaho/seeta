<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Intakes */

$this->title = 'Bio Data Form';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12">
            <h1><?= Html::encode($this->title) ?></h1>

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>