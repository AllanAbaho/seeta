<?php

/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = 'Seeta Applications System';
?>
<section class="about_top_wrapper">
    <div class="container">            
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-5">
                <div class="title">
                    <h2>Take The First Step To Learn With Us.</h2>
                    <p>At vero eos et accusamus et iusto odio dignissio ducimus qui blanditiis praesentium volu ptat umtjk deleniti atque corrupti quos esentium voluptatum delen itamus et iusto odio dignissimos ducimus qui blanditii. </p>
                 </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">            
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-7 ml-auto p-0">
                <div class="banner_learn">
                    <img src="<?=Url::to('@web/images/banner/learnstep.jpg', true); ?>" alt="" class="img-fluid">
                 </div>
            </div>
        </div>
    </div>
    <div class="items_shape"></div>
    <div class="story_about">
        <div class="container">            
            <div class="row">
                <div class="col-12 col-sm-6 col-md-7 col-lg-7">
                    <div class="story_banner">
                        <img src="<?=Url::to('@web/images/banner/about_us.png', true); ?>" alt="" class="img-fluid">
                     </div>
                </div>
                <div class="col-12 col-sm-6 col-md-5 col-lg-5">
                    <div class="about_story_title">
                        <h2>The Srory Of Our Eduwais.</h2>
                        <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum del Education atque corrupti.</p>
                     </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- End about_top_wrapper -->



<section class="events-area" id="ourevents">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="event_title_wrapper">  
                    <div class="sub_title">
                        <h2>Our Upcoming Events</h2>
                        <p>Lorem ipsum dolor sit amet mollis felis dapibus arcu donec viverra. Pede phasellus eget. Etiam maecenas vel vici quis dictum rutrum nec nisi et.</p>  
                    </div><!-- ends: .section-header -->
                    <div class="envent_all_view">
                        <a href="#" title="">View All</a>
                    </div>
                </div> 
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 events_full_box">
                <div class="events_single">
                    <div class="event_banner">
                        <a href="#"><img src="<?=Url::to('@web/images/events/event_1.jpg', true); ?>" alt="" class="img-fluid"></a>
                    </div>
                    <div class="event_info">
                        <h3><a href="#" title="">OneNote for Windows 10 Essential<br> Training</a></h3>
                        <div class="events_time">
                            <span class="time"><i class="flaticon-clock-circular-outline"></i>8.00 Am - 5.00 Pm</span>
                            <span><i class="fas fa-map-marker-alt"></i>Hall - A | Broklyn Audiitorium</span>
                        </div>
                        <p>Lorem ipsum dolor sit amet mollis dapibus arcur donec viverra to phasellus<br> eget. Etiam maecenas vel vici quis dictum rutrum nec nisi et. Ac pena<br> tibus aenean laoreet.</p>
                        <div class="event_dete">
                            <span class="date">09</span>
                            <span>Jan</span>
                        </div>
                    </div>
                </div>  
            </div>

            <div class="col-sm-12 events_full_box">
                <div class="events_single events_single_left">
                    <div class="event_info">
                        <h3><a href="#" title="">Magazine Design Start to Finish:<br> The Inside Pages</a></h3>
                        <div class="events_time">
                            <span class="time"><i class="flaticon-clock-circular-outline"></i>8.00 Am - 5.00 Pm</span>
                            <span><i class="fas fa-map-marker-alt"></i>Hall - A | Broklyn Audiitorium</span>
                        </div>
                         <p>Lorem ipsum dolor sit amet mollis dapibus arcur donec viverra to phasellus<br> eget. Etiam maecenas vel vici quis dictum rutrum nec nisi et. Ac pena<br> tibus aenean laoreet.</p>
                    </div>
                    <div class="event_banner">
                        <a href="#"><img src="<?=Url::to('@web/images/events/event_2.jpg', true); ?>" alt="" class="img-fluid"></a>
                    </div>
                    <div class="event_dete">
                        <span class="date">15</span>
                        <span>Jan</span>
                    </div>
                </div>  
            </div>

            <div class="col-sm-12 events_full_box">
                <div class="events_single">
                    <div class="event_banner">
                    <a href="#"><img src="<?=Url::to('@web/images/events/event_3.jpg', true); ?>" alt="" class="img-fluid"></a>
                    </div>
                    <div class="event_info">
                        <h3><a href="#" title="">Robotic Process Automation Tech<br> Primer</a></h3>
                        <div class="events_time">
                            <span class="time"><i class="flaticon-clock-circular-outline"></i>8.00 Am - 5.00 Pm</span>
                            <span><i class="fas fa-map-marker-alt"></i>Hall - A | Broklyn Audiitorium</span>
                        </div>
                         <p>Lorem ipsum dolor sit amet mollis dapibus arcur donec viverra to phasellus<br> eget. Etiam maecenas vel vici quis dictum rutrum nec nisi et. Ac pena<br> tibus aenean laoreet.</p>
                        <div class="event_dete">
                            <span class="date">20</span>
                            <span>Jan</span>
                        </div>
                    </div>
                </div>  
            </div>            

        </div>
    </div>
</section><!-- ./ End Events Area section -->



<section class="testimonial" id="alumni">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                <div class="testimonial_title">
                    <h2>What our Alumni Say About Us.</h2>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-8">
                <div class="testimonial_text_wrapper">
                    <div class="carousel_text slider-for">
                        <div>
                            <div class="single_box wow fadeInUp" data-wow-duration="2s" data-wow-delay=".2s">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque um sociis natoque pena pretium quis, sem.</p>
                            </div>
                        </div>
                        <div>
                            <div class="single_box wow fadeInUp" data-wow-duration="2s" data-wow-delay=".3s">
                                <p>Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus.</p>
                            </div>
                        </div>
                        <div>
                            <div class="single_box wow fadeInUp" data-wow-duration="2s" data-wow-delay=".4s">
                                <p>Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum.</p>
                            </div>
                        </div>
                    </div> 
                    <div class="reviewer_info">
                        <div class="carousel_images slider-nav">
                            <div class="restimonial_single_img">
                                <img src="<?=Url::to('@web/images/team/review_1.jpg', true); ?>" alt="" class="img-fluid">

                                <div class="name_position">
                                    <span class="name">Michael Jusi</span>
                                    <span>Graphic Student</span>
                                </div>
                            </div>                    
                            <div class="restimonial_single_img">
                            <img src="<?=Url::to('@web/images/team/review_4.jpg', true); ?>" alt="" class="img-fluid">
                                <div class="name_position">
                                    <span class="name">Jhon Smith</span>
                                    <span>Web Student</span>
                                </div>
                            </div>                    
                            <div class="restimonial_single_img ">
                            <img src="<?=Url::to('@web/images/team/review_3.jpg', true); ?>" alt="" class="img-fluid">
                                <div class="name_position">
                                    <span class="name">James Colins</span>
                                    <span>Graphic Student</span>
                                </div>
                            </div>                    
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section><!-- End Testimonial -->



<section class="blog" id="ourblog">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="sub_title">
                    <h2>Our Latest Blog</h2>
                    <p>Lorem ipsum dolor sit amet mollis felis dapibus arcu donec viverra. Pede phasellus eget. Etiam maecenas vel vici quis dictum rutrum nec nisi et.</p>  
                </div><!-- ends: .section-header -->
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                 <div class="single_item single_item_first">
                    <div class="blog-img">
                        <a href="#" title="">
                            <img src="<?=Url::to('@web/images/blog/blog_1.jpg', true); ?>" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="blog_title">
                        <span>LeaderShip Development</span>  
                        <h3><a href="#" title="">How to Become Master In <br>CSS within qa Week.</a></h3> 
                        <div class="post_bloger">
                            <span>15/02/2018 - By </span> <span class="bloger_name"> James Colins</span>
                        </div>               
                    </div>   
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                <div class="single_item single_item_center">
                     <div class="blog-img">
                        <a href="#" title="">
                            <img src="<?=Url::to('@web/images/blog/blog_2.jpg', true); ?>" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="blog_title">
                        <span>LeaderShip Development</span>  
                        <h3><a href="#" title="">Students work together to <br>solve a problem.</a></h3> 
                        <div class="post_bloger">
                            <span>15/02/2018 - By </span> <span class="bloger_name"> Jhon Deo</span>
                        </div>               
                    </div>   
                </div>
            </div>
            <div class="col-12 col-sm-6 col-md-4 col-lg-4">
               <div class="single_item single_item_last">
                     <div class="blog-img">
                        <a href="#" title="">
                            <img src="<?=Url::to('@web/images/blog/blog_3.jpg', true); ?>" alt="" class="img-fluid">
                        </a>
                    </div>
                    <div class="blog_title">
                        <span>LeaderShip Development</span>  
                        <h3><a href="#" title="">How to Become Master In <br>CSS within qa Week.</a></h3> 
                        <div class="post_bloger">
                            <span>15/02/2018 - By </span> <span class="bloger_name"> Simon Stain</span>
                        </div>               
                    </div>   
                </div>
            </div>
        </div>
    </div>
</section><!-- End Blog  -->


<section class="our_sponsor">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="sub_title">
                    <h2>Trusted To Tell Their Story</h2>
                    <p>Lorem ipsum dolor sit amet mollis felis dapibus arcu donec viverra. Pede phasellus eget. Etiam maecenas vel vici quis dictum rutrum nec nisi et.</p>  
                </div><!-- ends: .section-header -->
            </div>
            <div class="col-12 col-sm-12 col-md-12 col-lg-12">
                <ul class="sponsored_company_logos">
                    <li><img src="<?=Url::to('@web/images/logos/logo-1.png', true); ?>" alt="" class="img-fluid  wow fadeIn" data-wow-duration="2s" data-wow-delay=".1s"></li>
                    <li><img src="<?=Url::to('@web/images/logos/logo-2.png', true); ?>" alt="" class="img-fluid  wow fadeIn" data-wow-duration="2s" data-wow-delay=".2s"></li>
                    <li><img src="<?=Url::to('@web/images/logos/logo-3.png', true); ?>" alt="" class="img-fluid  wow fadeIn" data-wow-duration="2s" data-wow-delay=".3s"></li>
                    <li><img src="<?=Url::to('@web/images/logos/logo-4.png', true); ?>" alt="" class="img-fluid  wow fadeIn" data-wow-duration="2s" data-wow-delay=".6s"></li>
                    <li><img src="<?=Url::to('@web/images/logos/logo-5.png', true); ?>" alt="" class="img-fluid  wow fadeIn" data-wow-duration="2s" data-wow-delay=".5s"></li>
                </ul>
                <ul class="sponsored_company_logos sponsored_company_logos_2">
                    <li><img src="<?=Url::to('@web/images/logos/logo-6.png', true); ?>" alt="" class="img-fluid  wow fadeIn" data-wow-duration="2s" data-wow-delay=".4s"></li>
                    <li><img src="<?=Url::to('@web/images/logos/logo-7.png', true); ?>" alt="" class="img-fluid  wow fadeIn" data-wow-duration="2s" data-wow-delay=".3s"></li>
                    <li><img src="<?=Url::to('@web/images/logos/logo-8.png', true); ?>" alt="" class="img-fluid  wow fadeIn" data-wow-duration="2s" data-wow-delay=".6s"></li>
                    <li><img src="<?=Url::to('@web/images/logos/logo-9.png', true); ?>" alt="" class="img-fluid  wow fadeIn" data-wow-duration="2s" data-wow-delay=".5s"></li>
                </ul>
            </div>            
        </div>
    </div>
</section><!-- ./ End Our Sponsor -->
