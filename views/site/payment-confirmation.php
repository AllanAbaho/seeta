<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Intakes */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Payment Confirmation'];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12">
            <h1><?= Html::encode($this->title) ?></h1>

            <div class="row">
                <div class="col-md-6">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'name', 
                            'intake.name:ntext:Intake',
                            'intake.application_fee',
                            'nationality','religion','dob',
                            'created_at',
                            'updated_at',
                        ],
                    ]) ?>
                </div>
                <div class="col-md-6">
                    <h3><?= 'Payment Details'; ?></h3>
                    <?= DetailView::widget([
                        'model' => $payment,
                        'attributes' => [
                            'amount',
                            'phone_number',
                            'status',
                            [
                                'label'=>'Payer',
                                'value'=> function($data){
                                    return $data->first_name. ' '. $data->middle_name. ' '. $data->last_name;
                                }
                            ],
                            'created_at',
                            'updated_at',
                        ],
                    ]) ?>
                </div>        
            </div>
        </div>
    </div>
</div>
<script>
    function getStatus(){
        return jQuery.ajax({
            url : '<?= \yii\helpers\Url::to(['payment-status','id'=>$model->id],true);?>',
            type : 'GET',
            dataType:'json',
            success : function(res) {              
                if(res == 'PAID'){
                    alert('Payment has been received thank you for choosing Seeta High!')
                    window.location.href = "<?= \yii\helpers\Url::to(['index'],true);?>";
                }else{
                    console.log('Still Pending!')
                }
            },
        });
    }
    setInterval(getStatus, 30000);
</script>