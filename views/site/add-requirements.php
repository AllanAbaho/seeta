<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Intakes */

$this->title = $model->name;
// $this->params['breadcrumbs'][] = ['label' => 'Intakes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 col-lg-12">
            <h1><?= Html::encode($this->title) ?></h1>

            <p>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete Application', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this application?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
            <div class="row">
                <div class="col-md-6">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'name', 
                            'intake.name:ntext:Intake',
                            'intake.application_fee',
                            'nationality','religion','dob',
                            'created_at',
                            'updated_at',
                        ],
                    ]) ?>
                </div>
                <div class="col-md-6">
                    <h3><?= 'Intake Requirements'; ?></h3>
                    <?php if($dataProvider->count < 1): ?>
                        <?php $form = ActiveForm::begin(); ?>
                        <?php 
                            foreach($requirements as $index => $requirement){
                                echo $form->field($requirement, "[$index]value")->fileInput(['required'=>true])->label($requirement->name);
                            }
                        ?>
                        <div class="form-group">
                            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                        </div>
                        <?php ActiveForm::end(); ?>
                    <?php else: ?>
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                'requirement.name',
                                [
                                    'label' => 'Attachment',
                                    'value' => function($data){
                                        return Html::a('download file',['/download/index','file'=>$data->attachment]);
                                    },
                                    'format' => 'raw'
                                ],
                            ],
                        ]); ?>
                        <?= Html::a('Make Payment', ['make-payment', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?php endif; ?>
                </div>        
            </div>
        </div>
    </div>
</div>