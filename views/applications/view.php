<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Applications */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Applications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="applications-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'name',
                    'intake.name:text:Intake',
                    'dob',
                    'gender',
                    'religion',
                    'nationality',
                    'home_address',
                    'guardian_name',
                    'guardian_occupation',
                    'guardian_contact',
                    'status',
                    'created_at',
                    'updated_at',
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <h3>Required Documents</h3>
            <?= GridView::widget([
                'dataProvider' => $reqProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'requirement.name',
                    [
                        'label' => 'Attachment',
                        'value' => function($data){
                            return Html::a('download file',['/download/index','file'=>$data->attachment]);
                        },
                        'format' => 'raw'
                    ],
                    'created_at',
                ],
            ]); ?>
            <h3>Payment Details</h3>
            <?= GridView::widget([
                'dataProvider' => $payProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'amount',
                    'status',
                    'phone_number',
                    'created_at:text:Date'
                ],
            ]); ?>

        </div>
    </div>

</div>
