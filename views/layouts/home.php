<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\HomeAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\helpers\Url;

HomeAsset::register($this);
?>
<?php $this->beginPage() ?>

<!doctype html>
<html lang="en">


<head>
    <!-- Required meta tags -->
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="HTML,CSS,XML,JavaScript">
    <meta name="author" content="Ecology Theme">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="<?=Url::to('@web/images/favicon.ico', true); ?>" type="image/x-icon">
    <!-- Goole Font -->
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet"> 
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<header class="header_four">
<!-- Preloader -->
<div id="preloader">
    <div id="status">&nbsp;</div>
</div>    
    <div class="header_top">
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <div class="info_wrapper">
                        <div class="contact_info">                   
        					<ul class="list-unstyled">
                                <li><i class="flaticon-phone-receiver"></i>+256-700-460-055</li>
        						<li><i class="flaticon-mail-black-envelope-symbol"></i>admin@seeta.com</li>
        					</ul>                    
                        </div>
                        <div class="login_info">
                             <ul class="d-flex">
                                <li class="nav-item"><a href="<?=Url::to('/site/login',true); ?>" class="nav-link join_now js-modal-show"><i class="flaticon-padlock"></i>Log In</a></li>
                            </ul>
                            <a href="<?=Url::to('/site/apply-now'); ?>" title="" class="apply_btn">Apply Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="edu_nav">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-light bg-faded">
                <a class="navbar-brand" href="<?=Url::to('/site/index'); ?>">
                    <img src="<?=Url::to('@web/images/logo.png', true); ?>"/>
                </a>
                <div class="collapse navbar-collapse main-menu" id="navbarSupportedContent">
                    <ul class="navbar-nav nav lavalamp ml-auto menu">
                        <li class="nav-item"><a href="<?=Url::to('/site/index'); ?>" class="nav-link active">Home</a></li>
                        <li class="nav-item"><a href="<?=Url::to('/site/about'); ?>" class="nav-link">About</a></li>
                        <li class="nav-item"><a href="/site/index#ourevents" class="nav-link">Events</a></li>
                        <li class="nav-item"><a href="/site/index#ourblog" class="nav-link">Blog</a></li>
                        <li class="nav-item"><a href="/site/index#alumni" class="nav-link">Alumni</a></li>     
                        <li class="nav-item"><a href="<?=Url::to('/site/contact'); ?>" class="nav-link">Contact</a></li>
                    </ul>
                </div>
                <div class="mr-auto search_area ">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item"><i class="search_btn flaticon-magnifier"></i>
                            <div id="search">
                                <button type="button" class="close">×</button>
                                 <form>
                                     <input type="search" value="" placeholder="Search here...."  required/>
                                 </form>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav><!-- END NAVBAR -->
        </div> 
    </div>


    <!--==================
        Slider
    ===================-->

</header> <!--  End header section-->
<div class="container">
<?= Breadcrumbs::widget([
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]) ?>
<?= Alert::widget() ?>
</div>
<?= $content ?>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-3 col-md-3 col-lg-3 p-0 ">
                <div class="shape_wrapper">
                    <img src="<?=Url::to('@web/images/shapes/bubble_shpe_1.png', true); ?>" alt="" class="shape_t_1"> 
                    <img src="<?=Url::to('@web/images/shapes/round_shpae_1.png', true); ?>" alt="" class="shape_t_2">
                </div>   
            </div>
            <div class="col-12 col-sm-9 col-md-9 col-lg-9 p-0 become_techer_wrapper">
                <div class="become_a_teacher">
                    <div class="title">
                        <h2>Become A Student</h2>
                        <p>Lorem ipsum dolor sit amet mollis felis dapibus arcu donec viverra. Pede phasellus eget. Etiam maecenas vel vici quis dictum rutrum nec nisi et.</p>  
                    </div><!-- ends: .section-header -->
                    <div class="get_s_btn">
                        <a href="/site/apply-now" title="">Apply Now</a>
                    </div>
                    <img src="<?=Url::to('@web/images/shapes/bubble_shpe_2.png', true); ?>" alt="" class="shape_t_1"> 
                </div>                                
            </div>
        </div>
        <div class="footer_top">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="footer_single_col footer_intro">
                        <img src="<?=Url::to('@web/images/logo2.png', true); ?>" alt="" class="f_logo"> 
                        <p>Ante amet vitae vulputate odio nulla vel pretium pulvinar aenean. Rhoncus eget adipiscing etiam arcu. Ultricies justo ipsum nec amet.</p>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-2">
                    <div class="footer_single_col">
                        <h3>Useful Links</h3>
                        <ul class="location_info quick_inf0">
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Developers</a></li>
                            <li><a href="#">Services</a></li>
                            <li><a href="#">Contact</a></li>
                        </ul>                         
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-2">
                    <div class="footer_single_col information">
                        <h3>information</h3>
                        <ul class="quick_inf0">
                            <li><a href="#">Campus Tour</a></li>
                            <li><a href="#">Student Life</a></li>
                            <li><a href="#">Scholarship</a></li>
                            <li><a href="#">Admission</a></li>
                            <li><a href="#">Leadership</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="footer_single_col contact">
                        <h3>Contact Us</h3>
                        <p>Fell free to get in touch us via Phone or send us a message.</p>
                        <div class="contact_info">
                            <span>+256 700-460-055</span> 
                            <span class="email">admin@seeta.com</span>
                        </div>
                        <ul class="social_items d-flex list-unstyled">
                            <li><a href="#"><i class="fab fa-facebook-f fb-icon"></i></a></li>
                            <li><a href="#"><i class="fab fa-twitter twitt-icon"></i></a></li>
                            <li><a href="#"><i class="fab fa-linkedin-in link-icon"></i></a></li>
                            <li><a href="#"><i class="fab fa-instagram ins-icon"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="round_shape">
        <span class="shape_1"></span> 
        <span class="shape_2"></span> 
        <span class="shape_3"></span> 
        <span class="shape_4"></span> 
        <span class="shape_5"></span> 
        <span class="shape_6"></span>
    </div>
    <img src="<?=Url::to('@web/images/shapes/footer_bg_shpe_1.png', true); ?>" alt="" class="shapes1_footer"> 
</footer><!-- End Footer -->

<section id="scroll-top" class="scroll-top">
    <h2 class="disabled">Scroll to top</h2>
    <div class="to-top pos-rtive">
        <a href="#"><i class = "flaticon-right-arrow"></i></a>
    </div>
</section>

<?php $this->endBody() ?>
</body>


</html>
<?php $this->endPage() ?>
