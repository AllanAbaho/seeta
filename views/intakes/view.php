<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Intakes */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Intakes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="intakes-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <div class="row">
        <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'name',
                    'application_fee',
                    'created_at',
                    'updated_at',
                ],
            ]) ?>
        </div>
        <div class="col-md-6">
            <h3><?= 'Intake Requirements'; ?> <span><?= Html::a('Add', ['add-requirement', 'id' => $model->id], ['class' => 'btn btn-success']) ?><span></h3>
            <?php if($dataProvider->count > 0): ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'name',
                        'created_at',

                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{update}   {delete}',
                            'buttons' => [
                                'update' => function ($url, $model) {
                                    return Html::a('<i class="fas fa-edit"></i>', ['update-requirement', 'id'=>$model->id]);
                                },                        
                                'delete' => function ($url, $model) {
                                    return Html::a('<i class="fas fa-trash"></i>', ['delete-requirement', 'id'=>$model->id]);
                                },                        
                            ],
                        ],
                    ],
                ]); ?>
            <?php else: ?>
                <p>Please add this intake's requirements </p>
            <?php endif; ?>
        </div>
    </div>

</div>
