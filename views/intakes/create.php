<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Intakes */

$this->title = 'Add Intake';
$this->params['breadcrumbs'][] = ['label' => 'Intakes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="intakes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
