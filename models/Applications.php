<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "applications".
 *
 * @property int $id
 * @property int $intake_id
 * @property string $name
 * @property string $dob
 * @property string $gender
 * @property string $religion
 * @property string $nationality
 * @property string $home_address
 * @property string|null $guardian_name
 * @property string|null $guardian_occupation
 * @property string|null $guardian_contact
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class Applications extends \yii\db\ActiveRecord
{
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'applications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['intake_id', 'name', 'dob', 'gender', 'religion', 'nationality', 'home_address'], 'required'],
            [['intake_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['status','name', 'dob', 'gender', 'religion', 'nationality', 'home_address', 'guardian_name', 'guardian_occupation', 'guardian_contact'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'intake_id' => 'Intake',
            'name' => 'Name',
            'dob' => 'Dob',
            'gender' => 'Gender',
            'religion' => 'Religion',
            'nationality' => 'Nationality',
            'home_address' => 'Home Address',
            'guardian_name' => 'Guardian Name',
            'guardian_occupation' => 'Guardian Occupation',
            'guardian_contact' => 'Guardian Contact',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getIntake(){
        return $this->hasOne(Intakes::classname(), ['id'=>'intake_id']);
    }
}
