<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "applicant_requirements".
 *
 * @property int $id
 * @property int $application_id
 * @property int $requirement_id
 * @property string $attachment
 * @property string $created_at
 * @property string $updated_at
 */
class ApplicantRequirements extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'applicant_requirements';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['application_id', 'requirement_id', 'attachment'], 'required'],
            [['application_id', 'requirement_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['attachment'], 'string', 'max' => 100],
            ['file','file'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'application_id' => 'Application ',
            'requirement_id' => 'Requirement ',
            'attachment' => 'Attachment',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getApplication(){
        return $this->hasOne(Applications::classname(), ['id'=>'application_id']);
    }

    public function getRequirement(){
        return $this->hasOne(IntakeRequirements::classname(), ['id'=>'requirement_id']);
    }

}
