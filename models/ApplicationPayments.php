<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "application_payments".
 *
 * @property int $id
 * @property int $application_id
 * @property string $amount
 * @property string $phone_number
 * @property string|null $status
 * @property string $first_name
 * @property string|null $middle_name
 * @property string $last_name
 * @property string $reason
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class ApplicationPayments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'application_payments';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['application_id', 'amount', 'phone_number', 'first_name', 'last_name', 'reason'], 'required'],
            [['application_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['amount'], 'string', 'max' => 10],
            [['phone_number', 'status', 'first_name', 'middle_name', 'last_name', 'reason'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'application_id' => 'Application',
            'amount' => 'Amount',
            'phone_number' => 'Phone Number',
            'status' => 'Status',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'reason' => 'Reason',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
