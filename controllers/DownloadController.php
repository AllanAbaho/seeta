<?php

namespace app\controllers;

use yii\web\NotFoundHttpException;

class DownloadController extends \yii\web\Controller
{
    public function actionIndex($file)
    {
        $filepath = \Yii::getAlias('@app')."/uploads/" . $file;

        // Process download
        if(file_exists($filepath)) {
            //header('Content-Description: File Transfer');
            header('Content-Type: application/pdf');
            header('Content-Disposition: attachment; filename="'.basename($filepath).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filepath));
            //flush(); // Flush system output buffer
            readfile($filepath);
            return;
        }else{
            throw new NotFoundHttpException('The requested file does not exist.');
        }
        return $this->render('index');
    }
}
