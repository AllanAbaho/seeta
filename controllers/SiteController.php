<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Applications;
use app\models\IntakeRequirements;
use app\models\ApplicantRequirements;
use app\models\ApplicationPayments;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use Exception;

class SiteController extends Controller
{
    public $enableCsrfValidation = false;
    
    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public $layout = 'home';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/applications']);
        }
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/applications']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(['/applications']);
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/applications']);
        }
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Applications model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionApplyNow()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/applications']);
        }
        $model = new Applications();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                Yii::$app->session->setFlash('success','Details saved successfully. Please confirm to proceed!');
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('apply-now', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Applications model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/applications']);
        }
        $model = Applications::findOne($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success','Please attach required documents!');
            return $this->redirect(['add-requirements', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionMakePayment($id)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/applications']);
        }
        $callBackUrl = Url::to(['payment-confirmation','id'=>$id],true);
        $model = Applications::findOne($id);
        $payment = new ApplicationPayments();
        if ($this->request->isPost && $payment->load($this->request->post()) ) {
            $myValues = Yii::$app->request->post("ApplicationPayments");
            $schoolCode = '101';
            $password = 'SeetaApi';
            $hash = md5($schoolCode.$id.$password);
            $post = [
                'amount' => $myValues['amount'],
                'externalReference' => $id,
                'firstName'   => $myValues['first_name'],
                'middleName'   => $myValues['middle_name'],
                'lastName'   => $myValues['last_name'],
                'reason'   => $myValues['reason'],
                'phoneNumber'   => $myValues['phone_number'],
                'callBackUrl'   => $callBackUrl,
            ];

            $curl = curl_init();
            curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://schoolpaytest.servicecops.com/uatpaymentapi/AndroidRS/AdhocPayments/Request/'.$schoolCode.'/'.$hash,
            CURLOPT_SSLCERT => Yii::getAlias('@app') .'/certificates/cert.cer',
            CURLOPT_SSLKEY => Yii::getAlias('@app') .'/certificates/private.pem',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($post),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
            ));

            $response = curl_exec($curl);
            if ($response === false) {
                throw new Exception(curl_error($curl), curl_errno($curl));
            }
            curl_close($curl);
            $data = json_decode($response, true);
            if($data['returnCode'] == 0){
                $payment->application_id = $id;
                $payment->status = $data['status'];
                if(!$payment->save()){
                    print_r($payment->getFirstErrors());
                }
                $model->status = 'Submitted Payment';
                $model->save(false);
                $mailer = Yii::$app->mailer;
                $message = $mailer->compose('new-application-html')
                    ->setTo([Yii::$app->params['adminEmail']])      // or just $user->email
                    ->setFrom([Yii::$app->params['senderEmail'] => Yii::$app->params['senderName']])
                    ->setReplyTo(Yii::$app->params['senderEmail'])
                    ->setSubject('New Application')
                    ->send();
                Yii::$app->session->setFlash('success','Please hold on while your payment is being processed!');
                return $this->redirect(['payment-confirmation','id'=>$id]);    
            }else{
                Yii::$app->session->setFlash('error',$data['returnMessage']);
            }
        }

        return $this->render('make-payment', [
            'model' => $model,
            'payment' => $payment,
        ]);
    }

    public function actionPaymentConfirmation($id)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/applications']);
        }
        $model = Applications::findOne($id);
        $payment = ApplicationPayments::findOne(['application_id' => $id]);
        if ($this->request->isPost){
            $post = json_decode($this->request->getRawBody());
            if($post->returnCode == 0){
                $payment->status = 'PAID';
                $payment->save(false);   
                return $this->asJson([
                    'returnCode' => 0,
                    'status' => 'PAID'
                ]); 
            }
        }
        return $this->render('payment-confirmation', [
            'model' => $model,
            'payment' => $payment,
        ]);
    }

    public function actionPaymentStatus($id){
        $payment = ApplicationPayments::findOne(['application_id' => $id]);
        return json_encode($payment->status);
    }

    public function actionAddRequirements($id)
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/applications']);
        }
        $model = Applications::findOne($id);
        $requirements = IntakeRequirements::find()->where(['intake_id'=>$model->intake_id])->all();
        $dataProvider = new ActiveDataProvider([
            'query' => ApplicantRequirements::find()->where(['application_id'=>$id])
        ]);

        if (Model::loadMultiple($requirements, Yii::$app->request->post()) && Model::validateMultiple($requirements)) {
            foreach ($requirements as $index => $requirement) {
                $myReq = new ApplicantRequirements();
                $myReq->application_id = $id;
                $myReq->requirement_id = $requirement->id;
                $myReq->file = Uploadedfile::getInstance($requirement, "[$index]value");
                if ($myReq->file) {
                    $path = time().$myReq->file->baseName . '.' . $myReq->file->extension;
                    $myReq->attachment = $path;
                    if($myReq->save()){
                        $myReq->file->saveAs(Yii::getAlias('@app') .'/uploads/' . $path);
                    }else{
                        var_dump($myReq->getFirstErrors());
                        die();
                    }

                }
            }
            Yii::$app->session->setFlash('success','Please proceed to make payment!');
            return $this->redirect(['add-requirements', 'id' => $model->id]);
        }
        return $this->render('add-requirements', [
            'model' => $model,
            'requirements' => $requirements,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(['/applications']);
        }
        return $this->render('about');
    }
}
